﻿using System;
using System.Xml.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using static System.Collections.IEnumerable;


namespace Students
{
    public class Student
    {
        public string name;
        public string surname;
        public string FullName;
        public string Email;

        public Student(string Email) 
        {
            this.Email = Email;
 	    string pattern = "\\w+.\\w+@";
    	    var regex = new Regex(pattern);
    	    Match match = regex.Match(Email);
    	    if (match.Success)
    		{
     			FullName = match.Value.Replace(".", " ").Replace("@", "");
    		}
        }
        
        public Student(string name, string surname) 
        {
            this.name = name;
            this.surname = surname;
            FullName = name.ToLower() + " " + surname.ToLower();        
        }

    }
    
    internal class Program
    {
        static void Main(string[] args)
        {
            var student1c1 = new Student("karolina.batura@gmail.com");
            Console.WriteLine(student1c1.FullName);
            var student2c1 = new Student("ivan.zimin@mail.ru");
            Console.WriteLine(student2c1.FullName);
            var student3c1 = new Student("alex.savichev@ya.ru");
            Console.WriteLine(student3c1.FullName);

            var student1c2 = new Student("Karolina", "Batura");
            Console.WriteLine($"Student1: {student1c2.FullName}");
            var student2c2 = new Student("Ivan", "Zimin");
            Console.WriteLine($"Student2: {student2c2.FullName}");
            var student3c2 = new Student("Alex", "Savichev");
            Console.WriteLine($"Student3: {student3c2.FullName}");
            

            string[] subjects = {"Maths", "English", "Computing", "Design", "Drawing", "Geometry"};
        
            // Create a Random object  
            Random RandomSubjects = new Random();
            // Generate a random index less than the size of the array.  
            int randomIndex = RandomSubjects.Next(subjects.Length);
            // Display the result.  
            Console.WriteLine($"Randomly selected subjects is {subjects[randomIndex]}");
            
            Console.ReadKey();
            

            HashSet<string> hSet = new HashSet<string>(subjects);
            //Dictionary<Student, hSet> studentSubjectDict = null;
            //studentSubjectDict[student1c1] = RandomSubjects(subjects, 3);
            //studentSubjectDict.Add(student1c2, subjects);

        }
    }
}

